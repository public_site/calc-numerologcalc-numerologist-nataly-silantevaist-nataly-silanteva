import logo from './logo.svg';
import './App.css';
import "bootstrap/dist/css/bootstrap.css";

import Container from './component/Container/Container';
import CalcNumerologist from './component/CalcNumerologist/CalcNumerologist';
function App() {
  return (
    <div className='app'>
      <Container>
        <CalcNumerologist />
      </Container>
    </div>
  );
}

export default App;

import React from "react";
import styles from "./ResultCalc.module.scss";
import "./ResultCalc.module.scss";
import { CSSTransition } from "react-transition-group";

function ResultCalc(props) {
  var isSubmit = props.data.IsSubmit;
  console.log(isSubmit);
  var data = props.data;

  var classNames = {
    appear: styles.CSSTransitionAppear,
    appearActive: styles.CSSTransitionAppearActive,
    appearDone: styles.CSSTransitionAppearDone,
    enter: styles.CSSTransitionEnter,
    enterActive: styles.CSSTransitionEnterActive,
    enterDone: styles.CSSTransitionEnterDone,
    exit: styles.CSSTransitionExit,
    exitActive: styles.CSSTransitionExitActive,
    exitDone: styles.CSSTransitionExitDone,
  };

  return (
    <React.Fragment>
      <CSSTransition in={isSubmit} timeout={200} classNames={classNames}>
        <div className={`${styles.Container}`}>
          <div className="row">
            <div className="col-12 text-center">
              <h2 className={`h2 ${styles.ContainerTitle}`}>
                <b>Дата рождения:</b>
              </h2>
              <h2 className={`h2 ${styles.ContainerTitle}`}>
                <b>
                  <span>{data.ValueDate}</span>
                </b>
              </h2>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <table className={styles.TableCustom}>
                <tbody>
                  <tr>
                    <td colSpan="2" className={`${styles.CelSelected}`}>
                      <div className={styles.Card}>
                        <span>Доп. числа:</span>
                        <span className={styles.Value}>{data.NumberAdditionalNumber}</span>
                      </div>
                    </td>
                    <td className={`${styles.CelSelected}`}>
                      <div className={styles.Card}>
                        <span>Число судьбы:</span>
                        <span className={styles.Value}>{data.NumberFate}</span>
                      </div>
                    </td>
                    <td className={`${styles.CelSelected}`}>
                      <div className={`${styles.Card}`}>
                        <span>Темперамент</span>
                        <span className={styles.Value}>{data.temperament}</span>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div className={styles.Card}>
                        <span>Характер</span>
                        <span className={styles.Value}>{data.character}</span>
                      </div>
                    </td>
                    <td>
                      <div className={styles.Card}>
                        <span>Здоровье</span>
                        <span className={styles.Value}>{data.health}</span>
                      </div>
                    </td>
                    <td>
                      <div className={styles.Card}>
                        <span>Удача</span>
                        <span className={styles.Value}>{data.luck}</span>
                      </div>
                    </td>
                    <td className={`${styles.CelSelected}`}>
                      <div className={styles.Card}>
                        <span>Цель</span>
                        <span className={styles.Value}>{data.target}</span>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div className={styles.Card}>
                        <span>Энергия</span>
                        <span className={styles.Value}>{data.energy}</span>
                      </div>
                    </td>
                    <td>
                      <div className={styles.Card}>
                        <span>Логика</span>
                        <span className={styles.Value}>{data.logics}</span>
                      </div>
                    </td>
                    <td>
                      <div className={styles.Card}>
                        <span>Долг</span>
                        <span className={styles.Value}>{data.debt}</span>
                      </div>
                    </td>
                    <td className={`${styles.CelSelected}`}>
                      <div className={styles.Card}>
                        <span>Семья</span>
                        <span className={styles.Value}>{data.family}</span>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div className={styles.Card}>
                        <span>Интерес</span>
                        <span className={styles.Value}>{data.interest}</span>
                      </div>
                    </td>
                    <td>
                      <div className={styles.Card}>
                        <span>Труд</span>
                        <span className={styles.Value}>{data.work}</span>
                      </div>
                    </td>
                    <td>
                      <div className={styles.Card}>
                        <span>Память</span>
                        <span className={styles.Value}>{data.memory}</span>
                      </div>
                    </td>
                    <td className={`${styles.CelSelected}`}>
                      <div className={styles.Card}>
                        <span>Привычки</span>
                        <span className={styles.Value}>{data.habits}</span>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td className={`${styles.CelHide}`}></td>
                    <td>
                      <div className={styles.Card}>
                        <span>Быт</span>
                        <span className={styles.Value}>{data.everyday}</span>
                      </div>
                    </td>
                    <td colSpan={2} className={`${styles.CelWatermark}`}>
                      <div className={styles.CelWatermarkText}>
                        <p>
                          Nataly
                          <br />
                          Silanteva
                        </p>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </CSSTransition>
    </React.Fragment>
  );
}

export default ResultCalc;
